import { defineConfig,createFilter } from 'vite'
import vue from '@vitejs/plugin-vue'
import {optimize,Config} from 'svgo';
// 引入viteMockServe
import { viteMockServe } from 'vite-plugin-mock'
import svgoPlugin from 'vite-plugin-svgo'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': '/src',
      '@/components': '/src/components'
    }
  },
  server:{
    proxy:{
      '/api': {
          target: 'http://localhost:10001',
          changeOrigin: true,
          rewrite: (path:string) => path.replace(/^\/api/, ''),
        }
    }
  },
  plugins: [
    vue(),
    viteMockServe({
      mockPath: './src/mock',//设置模拟数据的存储文件夹
      enable: true,
      logger: true // 是否在控制台显示请求日志
    }),
    svgoPlugin({
      svgoConfig: './src/icons/svgo.yml',
    }),,
  ],
  optimizeDeps:{
    //exclude:['src/mock/*']
  }
})
