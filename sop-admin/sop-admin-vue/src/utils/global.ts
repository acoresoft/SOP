/*
注册全局方法
 */
import {ref} from 'vue'
import {useRoute,useRouter} from 'vue-router'
import axios from 'axios'
import { getToken, removeToken } from './auth'

// 创建axios实例
const client = axios.create({
  baseURL: import.meta.env.VUE_APP_BASE_API + '/api', // api 的 base_url
  timeout: 60000 // 请求超时时间,60秒
})

export const globalMethods =  {
  /**
   * 请求接口
   * @param uri uri，如：goods.get,goods.get/1.0
   * @param data 请求数据
   * @param callback 成功时回调
   * @param errorCallback 错误时回调
   */
  post: function(uri:string, data:any, callback:Function, errorCallback:Function) {
    const that = this
    const paramStr = JSON.stringify(data)
    if (!uri.endsWith('/')) {
      uri = uri + '/'
    }
    if (!uri.startsWith('/')) {
      uri = '/' + uri
    }
    client.post(uri, {
      data: encodeURIComponent(paramStr),
      access_token: getToken()
    }).then(function(response) {
      const resp = response.data
      const code = resp.code
      if (!code || code === '-9') {
        that.$message.error(resp.msg || '系统错误')
        return
      }
      if (code === '-100' || code === '18' || code === '21') { // 未登录
        that.logout()
        return
      }
      if (code === '0') { // 成功
        callback && callback.call(that, resp)
      } else {
        that.$message.error(resp.msg)
      }
    }).catch(function(error) {
      console.error('err' + error) // for debug
      errorCallback && errorCallback(error)
      that.$message.error(error.message)
    })
  },
  /**
   * tip，使用方式：this.tip('操作成功')，this.tip('错误', 'error')
   * @param msg 内容
   * @param type success / info / warning / error
   * @param stay 停留几秒，默认3秒
   */
  tip: function(msg:string, type:string, stay:string) {
    const that = this
    const stayInt = parseInt(stay) || 3
    that.$message({
      message: msg,
      type: type || 'success',
      duration: stayInt * 1000
    })
  },
  /**
   * 提醒框
   * @param msg 消息
   * @param okHandler 成功回调
   * @param cancelHandler
   */
  confirm: function(msg:string, okHandler:Function, cancelHandler:Function) {
    const that = this
    that.$confirm(msg, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning',
      beforeClose: (action, instance, done) => {
        if (action === 'confirm') {
          okHandler.call(that, done)
        } else if (action === 'cancel') {
          if (cancelHandler) {
            cancelHandler.call(that, done)
          } else {
            done()
          }
        } else {
          done()
        }
      }
    }).catch(function() {})
  },
  /**
   *  文件必须放在public下面
   * @param path 相对于public文件夹路径，如文件在public/static/sign.md，填：static/sign.md
   * @param callback 回调函数，函数参数是文件内容
   */
  getFile: function(path:string, callback:Function) {
    const that = this
    axios.get(path)
      .then(function(response) {
        callback.call(that, response.data)
      })
  },
  downloadText(filename:string, text:string) {
    const element = document.createElement('a')
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text))
    element.setAttribute('download', filename)

    element.style.display = 'none'
    document.body.appendChild(element)

    element.click()

    document.body.removeChild(element);
  },
  /**
   * 重置表单
   * @param formName 表单元素的ref
   */
  // resetForm(formName:string) {
  //   const frm = ref[formName]
  //   frm && frm.resetFields()
  // },
  logout: function() {
    const route=useRoute();
    const router=useRouter();
    removeToken()
    const fullPath = route.fullPath
    if (fullPath.indexOf('login?redirect') === -1) {
      router.push({ path: `/login?redirect=${fullPath}` })
    }
  }
}
