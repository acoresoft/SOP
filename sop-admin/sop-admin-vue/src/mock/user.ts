import { MockMethod,RespThisType, MockConfig } from 'vite-plugin-mock'


const tokens = {
  admin: {
    token: 'admin-token'
  },
  editor: {
    token: 'editor-token'
  }
}
interface UserInfo {
  roles: string[];
  introduction: string;
  avatar: string;
  name: string;
}

const users: Record<string, UserInfo> = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin'
  },
  'editor-token': {
    roles: ['editor'],
    introduction: 'I am an editor',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
};
export default function (){

  return [{
    url: '/api/user/get',
    method: 'get',
    response: (that:any) => {
      console.log(that.query.token);
      const token:string= that.query?.token;
      const info = users[token]
      // mock error
      if (!info) {
        return {
          code: 50008,
          message: '登录失败，无法获取用户详情。'
        }
      }
      return {
        code: 20000,
        data: info
      }
    }
}]}
