export default function () {
  return [
    {
      url: '/api/service/instance/list',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: []
        }
      },
    },
    {
      url: '/api/isp/sdk/list',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: []
        }
      },
    },
    {
      url: '/api/ip/blacklist/page',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: []
        }
      },
    },
    {
      url: '/api/registry/service/list',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: ['upms-service','openapi-service']
        }
      },
    },
    {
      url: '/api/monitornew/data/page',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: []
        }
      },
    },
    {
      url: '/api/route/page',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: []
        }
      },
    },
    {
      url: '/api/config/limit/list',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: []
        }
      },
    },
    {
      url: '/api/route/list/1.2',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: []
        }
      },
    },
  ]
}
