export default function () {
  return [
    {
      url: '/api/isv/keys/get',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: [{
            id:'1'
          }]
        }
      },
    },
    {
      url: '/api/role/listAll',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: [{
            roleCode:'normal',
            roleName:'普通权限',
            roleDescription:''
          },{
            roleCode:'vip',
            roleName:'VIP权限',
            roleDescription:''
          }]
        }
      },
    },
    {
      url: '/api/isv/info/page',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: {
            total:100,
            list:[{
              id:1,
              appKey:'20240808000001'
            }]
          }
        }
      },
    },
    {
      url: '/api/role/page',
      method: 'get',
      response: (req:any) => {
        return {
          code: 20000,
          data: {}
        }
      },
    },
  ]
}
