import { MockMethod, MockConfig } from 'vite-plugin-mock'
// export default [
//   {
//     url: '/api/get',
//     method: 'get',
//     response: ({ query }) => {
//       return {
//         code: 0,
//         data: {
//           name: 'xiao ming',
//         },
//       }
//     },
//   },
//   {
//     url: '/api/post',
//     method: 'post',
//     timeout: 2000,
//     response: {
//       code: 0,
//       data: {
//         name: 'xiao ming',
//       },
//     },
//   },
//   {
//     url: '/api/text',
//     method: 'post',
//     rawResponse: async (req, res) => {
//       let reqBody = ''
//       await new Promise((resolve) => {
//         req.on('data', (chunk) => {
//           reqBody += chunk
//         })
//         req.on('end', () => resolve(undefined))
//       })
//       res.setHeader('Content-Type', 'text/plain')
//       res.statusCode = 200
//       res.end(`hello, ${reqBody}`)
//     },
//   },
// ] as MockMethod[]

export default function (config: MockConfig) {
  return [
    {
      url: '/api/text',
      method: 'post',
      rawResponse: async (req, res) => {
        let reqBody = ''
        await new Promise((resolve) => {
          req.on('data', (chunk) => {
            reqBody += chunk
          })
          req.on('end', () => resolve(undefined))
        })
        res.setHeader('Content-Type', 'text/plain')
        res.statusCode = 200
        res.end(`hello, ${reqBody}`)
      },
    },
  ]
}
