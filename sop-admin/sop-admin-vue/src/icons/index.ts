import {App,defineAsyncComponent} from 'vue'
import SvgIcon from '@/components/SvgIcon/index.vue'// svg component

// register globally
const install = (app: App): void => {
  app.component('SvgIcon', SvgIcon);

  // 动态加载 SVG 图标
  const icons = import.meta.glob('./svg/**/*.svg', { eager: true });
  // 用于存放所有 <symbol> 元素的 <defs> 元素
  const svgDefs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
  // 遍历并注册每个 SVG 文件
  Object.entries(icons).forEach(([filePath, module]) => {
    console.log(filePath);
    console.log(module);
    const iconName = filePath.split('/').pop()?.replace(/\.\w+$/, '') || '';
    // //@ts-ignore
    // const iconComponent = defineAsyncComponent(() => module.default);
    // console.log(iconComponent);
    // const componentName = `Icon${iconName.charAt(0).toUpperCase() + iconName.slice(1)}`;
    // console.log(componentName);
    // //@ts-ignore
    // app.component(componentName, module.default);


    // 遍历 glob 对象，将每个 SVG 图标解析为 <symbol> 并插入到 <defs> 元素中
    //@ts-ignore
    const content = module.default;
    const symbol = new DOMParser().parseFromString(content, 'image/svg+xml').documentElement;
    symbol.id = `icon-${iconName}`;
    svgDefs.appendChild(symbol);

    // 将 <defs> 元素插入到文档中
    document.body.appendChild(svgDefs);
  });
};
export default install;
