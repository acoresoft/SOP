import {createMemoryHistory, createRouter} from 'vue-router'


/* Layout */
import Layout from '@/layout/index.vue'

const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404.vue'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index.vue'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/service',
    component: Layout,
    name: 'Service',
    meta: { title: '服务管理', icon: 'example', open: true },
    children: [
      {
        path: 'list',
        name: 'ServiceList',
        component: () => import('@/views/service/serviceList.vue'),
        meta: { title: '服务列表' }
      },
      {
        path: 'route',
        name: 'Route',
        component: () => import('@/views/service/sopRoute.vue'),
        meta: { title: '路由管理' }
      },
      {
        path: 'monitor',
        name: 'Monitor',
        component: () => import('@/views/service/monitorNew.vue'),
        meta: { title: '路由监控' }
      },
      {
        path: 'limit',
        name: 'Limit',
        component: () => import('@/views/service/sopLimit.vue'),
        meta: { title: '限流管理' }
      },
      {
        path: 'blacklist',
        name: 'Blacklist',
        component: () => import('@/views/service/ipBlacklist.vue'),
        meta: { title: 'IP黑名单' }
      },
      {
        path: 'sdk',
        name: 'Sdk',
        component: () => import('@/views/service/sopSdk.vue'),
        meta: { title: 'SDK管理' }
      }
    ]
  },

  {
    path: '/isv',
    component: Layout,
    name: 'Isv',
    meta: { title: 'ISV管理', icon: 'user', open: true },
    children: [
      {
        path: 'list',
        name: 'IsvList',
        component: () => import('@/views/isv/index.vue'),
        meta: { title: 'ISV列表' }
      },
      {
        path: 'role',
        name: 'Role',
        component: () => import('@/views/isv/role.vue'),
        meta: { title: '角色管理' }
      },
      {
        path: 'keys',
        name: 'Keys',
        component: () => import('@/views/isv/keys.vue'),
        hidden: true,
        meta: { title: '秘钥管理' }
      }
    ]
  },
  // 404 page must be placed at the end !!!
  //{ path: '*', redirect: '/404', hidden: true }
];

const router = createRouter({
  history: createMemoryHistory(),
  routes:constantRoutes
});

export default router
