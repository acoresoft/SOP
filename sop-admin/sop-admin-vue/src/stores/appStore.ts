import { defineStore } from 'pinia';

export const useAppStore = defineStore('app', {
  state: () => ({
    count: 0,
    sidebar:{
      opened:true,
      withoutAnimation:true
    },
    device:'',
    settings:{
      sidebarLogo:true,
      fixedHeader:true
    }}),
  getters: {
    doubleCount: (state) => state.count * 2,
  },
  actions: {
    increment() {
      this.count++;
    },
  },
});
