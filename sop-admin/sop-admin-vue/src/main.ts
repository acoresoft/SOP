import { createApp } from "vue"

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css';
import zhCn from 'element-plus/es/locale/lang/zh-cn'; // 引入中文语言包

import '@/styles/index.scss' // global css

import App from './App.vue'
import router from './router'
import pinia from './stores';
import { globalMethods } from './utils/global';
import {dateStr, dateTimeStr, parseDate, parseTime, timeStr} from '@/utils/formatTime';

import SvgIcon1 from '@/components/SvgIcon1/index.vue';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
 // icon/ permission control
//import '@/utils/global' // 自定义全局js
import installSvgIcon from '@/icons'
/**
 * If you don't want to use mock-server
 * you want to use mockjs for request interception
 * you can execute:
 *
 * import { mockXHR } from '../mock'
 * mockXHR()
 */

const app = createApp(App);
app.use(ElementPlus,{locale:zhCn});
if (router) {
  app.use(router);
}
app.use(pinia)
installSvgIcon(app)
// 动态加载 SVG 文件
const svgFiles = import.meta.glob('@/icons/svg/*.svg', { eager: true });
app.component('SvgIcon1', SvgIcon1)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
//全局方法挂载
Object.keys(globalMethods).forEach((key:string) => {
  //@ts-ignore
  app.config.globalProperties[`$${key}`] = globalMethods[key];
});
app.config.globalProperties.parseTime = parseTime;
app.config.globalProperties.parseDate = parseDate;
app.config.globalProperties.dateTimeStr = dateTimeStr;
app.config.globalProperties.dateStr = dateStr;
app.config.globalProperties.timeStr = timeStr;
app.config.globalProperties.baseURL = import.meta.env.VITE_API_URL;
//console.log(app.config.globalProperties)
app.mount('#app');
