module.exports = {
  presets: [
    //'@vue/app',
    ["@babel/preset-env", {
      "modules": true // 这告诉 Babel 不要转换模块语法
    }]
  ],
  "plugins": [
    "@babel/plugin-transform-modules-commonjs",
    "@babel/plugin-syntax-dynamic-import" // 支持动态导入
  ]
}
